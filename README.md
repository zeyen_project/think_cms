# think_cms

#### 介绍
基于thinkphp5.1 和 LayUI的开源一套纯净后台管理系统，界面简洁、清新、美观，包含后台用户，角色权限，菜单路由等功能，方便你在此系统的基础上开发出更加庞大的系统。

#### 软件架构
ThinkPHP5.1 + LayUi


#### 安装教程

1. 下载项目代码(通过composer create-project zeyen/think_cms创建 或者git clone [https://gitee.com/zeyen_project/think_cms.git](https://gitee.com/zeyen_project/think_cms.git) 进行clone下来都可以)；

2. 导入项目根目录下的think_cms.sql文件到MySQL中，然后修改.env文件中的一些配置 ；

3. **由于未上传Vendor文件夹，因此需要使用者配置composer.json文件，当然本项目的composer.json文件Zeyen已配置好，只需在根目录下执行下 compose update命令进行项目包文件的更新和安装；**

4. 将域名解析到public目录；（注意：此框架进行了配置，可以使用两个域名绑定前后端目录，详情请见.env中的域名配置说明）

5. 最好是配置一下伪静态，这里Zeyen提供下Nginx下的配置

   ```
    location / {
               index index.php index.html error/index.html;
               autoindex  off;
               if (!-e $request_filename) {
                   rewrite  ^(.*)$  /index.php?s=/$1  last;
                   break;
               }
           }
   ```

   

#### 使用说明

1.  该项目实现了前后台自定义域名，配置在.env文件中，后台——adminer  前台——index
2.  改项目实现了全路由处理，因此传统的访问方式在这里不可行，请阅读文档或者源码进行操作；
3.  注意两个基类
    - Adminbase 严格的权限检查
     - NotAuth 需要登陆但是不需要权限检查

#### 账号密码

1. ​	账号密码：（当前仅一个超级管理员账号）

    - 账号：admin

    - 密码：123456



#### 项目展示

1. 登录页

   ![](https://z3.ax1x.com/2021/05/25/2S1OBQ.png)

2. 首页

   ![](https://z3.ax1x.com/2021/05/25/2S393V.png)

3. 功能页

   ![](https://z3.ax1x.com/2021/05/25/2S3CcT.png)



#### 作者信息

1.  Zeyen  邮箱 zeyen0186@aliyun.com
2.  Zeyen 官方博客 [www.zeyen.cn](https://www.zeyen.cn)
3.  你可以 [https://gitee.com/athenazetan](https://gitee.com/athenazetan) 这个地址来了解 Zeyen的其他优秀开源项目

#### 特别鸣谢

特别感谢 Layui, ThinkPHP 以及开发者xiucaiwu提供的包文件。
