<?php
namespace app\adminer\controller;

use think\facade\Request;
use app\adminer\model\AuthUser;

/**
 * @author Zeyen <zeyen0186@aliyun.com>
 * @date 2021-05-24
 * Class Login
 * @package app\adminer\controller
 */
class Login extends Adminbase
{
    public function index() {
        if($this->request->isPost()) {
            $username = Request::post('userName');
            $password = Request::post('password');
            $captcha = Request::post('captcha');
            $remember = Request::post('rememberMe');

            if(!captcha_check($captcha)) {
                $this->resultData('$_2');
            }
            $user_info = AuthUser::login($username, $password);
            if($user_info) {
                $user_session_info = [
                    'id'        => $user_info['id'],
                    'username'  => $user_info['username'],
                    'super'     => $user_info['super'],
                ];
                session('user_info', $user_session_info);
                $this->resultData('$_0');
            }
            $this->resultData('$_3');
        }
        $userInfo = session('user_info');
        if($userInfo) {
            $this->redirect(url('admin.index'));
        }
        return $this->fetch('index');
    }

}