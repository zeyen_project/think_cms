/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : think_cms

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 25/05/2021 19:44:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for my_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `my_auth_group`;
CREATE TABLE `my_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` varchar(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='用户组表';

-- ----------------------------
-- Records of my_auth_group
-- ----------------------------
BEGIN;
INSERT INTO `my_auth_group` VALUES (6, '管理员', 1, '1,2');
COMMIT;

-- ----------------------------
-- Table structure for my_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `my_auth_group_access`;
CREATE TABLE `my_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组明细表';

-- ----------------------------
-- Records of my_auth_group_access
-- ----------------------------
BEGIN;
INSERT INTO `my_auth_group_access` VALUES (1, 1);
INSERT INTO `my_auth_group_access` VALUES (1, 2);
INSERT INTO `my_auth_group_access` VALUES (2, 1);
INSERT INTO `my_auth_group_access` VALUES (2, 2);
INSERT INTO `my_auth_group_access` VALUES (3, 6);
COMMIT;

-- ----------------------------
-- Table structure for my_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `my_auth_rule`;
CREATE TABLE `my_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` mediumint(9) DEFAULT '0',
  `pth` varchar(30) DEFAULT '' COMMENT '记录菜单的层级关系',
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `sort` tinyint(4) DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:显示 0:隐藏',
  `condition` char(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='规则表';

-- ----------------------------
-- Records of my_auth_rule
-- ----------------------------
BEGIN;
INSERT INTO `my_auth_rule` VALUES (1, 0, '0,1', '', '首页', 1, 0, 1, '');
INSERT INTO `my_auth_rule` VALUES (2, 1, '0,1,2', 'admin.index.welcome', '欢迎', 1, 0, 1, '');
INSERT INTO `my_auth_rule` VALUES (14, 0, '0,14', '', '权限', 1, 3, 1, '');
INSERT INTO `my_auth_rule` VALUES (16, 14, '0,14,16', 'admin.system.list', '菜单管理', 1, 0, 1, '');
INSERT INTO `my_auth_rule` VALUES (17, 14, '0,14,17', '', '权限管理', 1, 0, 1, '');
INSERT INTO `my_auth_rule` VALUES (18, 17, '0,14,17,18', 'admin.member.index', '成员管理', 1, 0, 1, '');
INSERT INTO `my_auth_rule` VALUES (36, 32, '0,14,17,32,36', 'admin.group.add', '增加角色', 1, 0, 0, '');
INSERT INTO `my_auth_rule` VALUES (32, 17, '0,14,17,32', 'admin.group.index', '角色管理', 1, 0, 1, '');
INSERT INTO `my_auth_rule` VALUES (37, 32, '0,14,17,32,37', 'admin.group.edit', '编辑角色', 1, 1, 0, '');
INSERT INTO `my_auth_rule` VALUES (38, 32, '0,14,17,32,38', '', '禁用角色', 1, 2, 0, '');
INSERT INTO `my_auth_rule` VALUES (39, 32, '0,14,17,32,39', 'admin.group.grant', '授权角色', 1, 3, 0, '');
INSERT INTO `my_auth_rule` VALUES (40, 16, '0,14,16,40', 'admin.system.add', '添加菜单', 1, 0, 0, '');
INSERT INTO `my_auth_rule` VALUES (41, 16, '0,14,16,41', 'admin.system.edit', '编辑菜单', 1, 1, 0, '');
INSERT INTO `my_auth_rule` VALUES (42, 16, '0,14,16,42', 'admin.system.delete', '删除菜单', 1, 2, 0, '');
INSERT INTO `my_auth_rule` VALUES (43, 18, '0,14,17,18,43', 'admin.member.add', '管理员添加', 1, 0, 0, '');
INSERT INTO `my_auth_rule` VALUES (44, 18, '0,14,17,18,44', 'admin.member.edit', '管理员编辑', 1, 1, 0, '');
INSERT INTO `my_auth_rule` VALUES (45, 18, '0,14,17,18,45', 'admin.index.editPassword', '管理员改密', 1, 2, 0, '');
INSERT INTO `my_auth_rule` VALUES (46, 18, '0,14,17,18,46', 'admin.member.grant', '管理员授权', 1, 3, 0, '');
COMMIT;

-- ----------------------------
-- Table structure for my_auth_user
-- ----------------------------
DROP TABLE IF EXISTS `my_auth_user`;
CREATE TABLE `my_auth_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `phone` char(11) DEFAULT NULL COMMENT '操作管理员的手机号码',
  `salt` varchar(8) NOT NULL DEFAULT '' COMMENT '随机因子',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 1:启用 0:禁止',
  `super` tinyint(1) DEFAULT '0' COMMENT '1:超级管理员 0:普通管理员',
  `remark` varchar(255) NOT NULL COMMENT '备注说明',
  `create_at` datetime DEFAULT NULL COMMENT '创建时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(15) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `last_location` varchar(100) NOT NULL DEFAULT '0' COMMENT '最后登录位置',
  PRIMARY KEY (`id`),
  KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='AUTH-权限管理-后台管理员用户表';

-- ----------------------------
-- Records of my_auth_user
-- ----------------------------
BEGIN;
INSERT INTO `my_auth_user` VALUES (1, 'admin', 'b62581104c99079c5b14ec51979b1b59', '', 'voBMjD', 1, 1, '超级管理员', '2018-04-12 20:27:20', '0000-00-00 00:00:00', '0', '0');
INSERT INTO `my_auth_user` VALUES (3, 'zeyen', '545a79773acb4f6f062e80e67f41e3f3', NULL, 'if4UNy', 1, 0, '管理员', '2021-05-25 16:50:01', NULL, '0', '0');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
